#include <iostream>

class Vector
{
public:
    Vector() : x(15), y(20), z(25)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double Show()
    {
        double sum = pow(x, 2) + pow(y, 2) + pow(z, 2);

        return sqrt(sum);
    }

private:
    double x;
    double y;
    double z;
};


int main()
{
    Vector v;
    std::cout << "Result:" << " " << v.Show();
}

